using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [Tooltip("板子类型")]
    public PlatFormType platFormType;
    [Tooltip("弹跳速度")]
    public float bounceSpeed = 4f;
    /// <summary>
    /// 进入碰撞检测区
    /// </summary>
    /// <param name="collisionInfo">谁与挂载了这个脚本的物体进行了碰撞</param>
    void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        if (collisionInfo.contacts[0].normal == Vector2.down)
        {
            //如果碰撞物体与挂载这个脚本的物体垂直碰撞
            //获取该碰撞物体身上的组件
            Rigidbody2D rb = collisionInfo.gameObject.transform.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.velocity = Vector2.up * bounceSpeed;
            }
            if (platFormType == PlatFormType.weak)
            {
                if (GetComponent<Animator>() != null)
                {
                    //Play animation
                    //Destroy the platform
                    GetComponent<Animator>().SetTrigger("Trigger");
                    Invoke(nameof(SetActive),0.4f);

                }
            }
        }
    }

    void SetActive(){
        gameObject.SetActive(false);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("MainCamera")){
            gameObject.SetActive(false);
        }
    }
}
//板子类型
[SerializeField]
public enum PlatFormType
{
    normal, weak
}
