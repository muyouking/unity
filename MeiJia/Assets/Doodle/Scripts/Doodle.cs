using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Doodle角色控制
/// </summary>
public class Doodle : MonoBehaviour
{
    public float moveSpeed;
    public Rigidbody2D rb;

    Touch initTouch;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        KeyBoardControlloer();

        FingerController();
    }
/// <summary>
/// 键盘控制
/// </summary>
    void KeyBoardControlloer()
    {
        float H = Input.GetAxisRaw("Horizontal");
        // float V = Input.GetAxisRaw("Vertical");
        rb.velocity = new Vector2(H * moveSpeed, rb.velocity.y);

        //Doodle的方向调
        if (H != 0)
        {
            transform.localScale = new Vector3(H, 1, 1);
        }

    }
    /// <summary>
    /// 触碰控制
    /// </summary>
    void FingerController()
    {
        //记录当前触碰点数
        int TouchCount = Input.touchCount;

        //一只
        if (TouchCount == 1)
        {
            Touch t = Input.touches[0];
            //开始
            if (t.phase == TouchPhase.Began)
            {
                Debug.Log("手指开始触碰屏幕: Index" + t.fingerId);
                Debug.Log("State:" + t.phase.ToString());

            }
            //Move
            if (t.phase == TouchPhase.Moved)
            {
                Debug.Log("手指在屏幕移动: Index" + t.fingerId);
                Debug.Log("State:" + t.phase.ToString());
                Debug.Log("手指当前位置:" + t.position);
                Debug.Log("deltaPosition:" + t.deltaPosition);
                Debug.Log("deltaTime:" + t.deltaTime);
                

            }

            //end
            if (t.phase == TouchPhase.Ended)
            {
                Debug.Log("手指抬起");
            }

        }

        //两只
        if (TouchCount == 2)
        {

            //开始

            //Move

            //end

        }

        //多只
        if (TouchCount > 2)
        {

            //开始

            //Move

            //end

        }
    }
}
