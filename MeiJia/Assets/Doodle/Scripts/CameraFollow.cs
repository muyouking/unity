using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.3f;
    Vector3 currentVelocity;
    void LateUpdate()
    {
        if(target.position.y > transform.position.y){
            Vector3 targetPos = new Vector3(0,target.position.y,-1);
            transform.position = Vector3.SmoothDamp(transform.position,targetPos,ref currentVelocity,smoothSpeed * Time.deltaTime);
        }
    }

    
}
