using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [Tooltip("platform的种类")]
    public GameObject[] platFormPrefabs;
    public float currentYPos = 0f;
    [Tooltip("生成的platform离相机中心点多远")]
    public float CameHight = 4.8f;

    [Tooltip("保留所有platform的池子")]
    public Transform platformPool;
    Camera _camera;
    // Start is called before the first frame update
    void Start()
    {
        SpawPlatformPool();
        _camera = Camera.main;
       
        // for(int i = 0;i<10;i++){
        //     PickNewPlatForm();
        // }
    }

    // Update is called once per frame
    void Update()
    {
         if(currentYPos < _camera.transform.position.y + CameHight ){
            PickNewPlatForm();
        }
    }

    /// <summary>
    /// 初始化PlatForm池子
    /// </summary>
    void SpawPlatformPool(){
        int basePlatformAmount = 30;
        int weakPlatformAmount = 15;

        for (int i = 0;i<basePlatformAmount;i++){
            GameObject platform = Instantiate(platFormPrefabs[0],platformPool);
            platform.SetActive(false);
        }
         for (int i = 0;i<weakPlatformAmount;i++){
            GameObject platform = Instantiate(platFormPrefabs[1],platformPool);
            platform.SetActive(false);
        }
    }

    void PickNewPlatForm(){
        currentYPos += Random.Range(0.3f,1.3f);
        float xPos = Random.Range(-3.2f,3.2f);      //板子在x轴上的随机位置
        int r = 0;
        do{
            r = Random.Range(0,platformPool.childCount);

        }while(platformPool.GetChild(r).gameObject.activeInHierarchy);

        platformPool.GetChild(r).position = new Vector2(xPos,currentYPos);
        platformPool.GetChild(r).gameObject.SetActive(true);
    }
}
