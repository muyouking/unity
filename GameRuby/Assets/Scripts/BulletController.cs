﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
///<summary>
///控制子弹的移动，碰撞
///</summary>
public class BulletController : MonoBehaviour
{
    Rigidbody2D rbody;
    public AudioClip audioClip;
    // Start is called before the first frame update
   void Awake() 
    {
    rbody = GetComponent<Rigidbody2D>();    
    Destroy(this.gameObject,2f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //子弹的移动
    public void Move(Vector2 moveDirector,float moveForce){
        rbody.AddForce(moveDirector * moveForce,ForceMode2D.Force);
    }
    private void OnCollisionEnter2D(Collision2D other) {
        //检测是否碰撞到敌人
        EnemyController ec = other.gameObject.GetComponent<EnemyController>();
        if (ec != null){
            Debug.Log("碰撞到敌人");
            // Destroy(ec.gameObject);
            AudioManager.instance.AudioPlay(audioClip);     //播放命中敌人的音效
            ec.Fixed();
        }
        Destroy(this.gameObject);
    }
}
