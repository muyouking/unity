﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
///<summary>
///草莓被玩家碰撞时检测相关
///</summary>
public class Collectible : MonoBehaviour
{
    // Start is called before the first frame update
    public ParticleSystem collectEffect;    //拾取特效
    public AudioClip audioClip;         //拾取音效
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other) {
        PlayerController pc = other.GetComponent<PlayerController>();
        if ( pc != null){
            // Debug.Log("碰撞到玩家");
            if (pc.MyCurrentHealth < pc.MyMaxHelath){
                pc.changeHealth(1);
                Instantiate(collectEffect,transform.position,Quaternion.identity);
                AudioManager.instance.AudioPlay(audioClip);
                Destroy(this.gameObject);
            }
        }
    }
}
