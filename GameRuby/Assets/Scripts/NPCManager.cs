﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NPCManager : MonoBehaviour
{
    public GameObject dialogImage;  //对话框
    public float showTime = 4f;     //对话框显示时间
    private float showTimer;        //对话框显示计时器
    // Start is called before the first frame update
    void Start()
    {
        dialogImage.SetActive(false);   //初始时默认隐藏对话框
        showTimer = -1;
    }

    // Update is called once per frame
    void Update()
    {
        showTimer -= Time.deltaTime;    //倒计时
        if (showTimer < 0){
            dialogImage.SetActive(false);   //计时器时间小于0就隐藏对话框
        }
    }
   public void ShowDialog(){
        showTimer = showTime;
        dialogImage.SetActive(true);        //显示对话框
    }
}
