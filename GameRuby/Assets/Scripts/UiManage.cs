﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManage : MonoBehaviour
{
    //角度的血条
  public Image healthBar;

  //单例模式
public static UiManage instance{get;private set;}
//更新血条
void  Awake() {
    instance = this;
}
  public void UpdateHealthBar(int curAmount,int maxAmount){
      healthBar.fillAmount = (float)curAmount/(float)maxAmount;
  }
}
