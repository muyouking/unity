﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
///<summary>
///玩家受到伤害时的碰撞检测
///</summary>
public class DamageArea : MonoBehaviour
{
    
    private  void OnTriggerStay2D(Collider2D other) {
        PlayerController pc = other.GetComponent<PlayerController>();
        if (pc != null){
            if (pc.MyCurrentHealth>0){
                pc.changeHealth(-1);
                
            }
        }
    }    
    

}
