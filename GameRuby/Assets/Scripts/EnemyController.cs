﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///敌人控制相关
///</summary>
public class EnemyController : MonoBehaviour
{
    public float speed=3;

    public float changeDirectionTime = 2f;      //改变方向的时间
    private float changeTimer;   //改变方向的计时器  
    private Rigidbody2D rbody;

    private Vector2 moveDirection;      //移动方向
    // Start is called before the first frame update
    public bool isVertical;             //是否是垂直方向移动

    //动画控制
    private Animator anim;

    //是否被修复 
    private bool IsFixed;
    //损坏的烟雾特效
    public ParticleSystem brokenEffect;
    //机器人被修复的音效
    public AudioClip audioClip;
    void Start()
    {
        changeTimer = changeDirectionTime;
        rbody = GetComponent<Rigidbody2D>();
        moveDirection = isVertical ? Vector2.up : Vector2.right;      //根据这个isVertical来决定是上下移动还是左右移动
        anim =gameObject.GetComponent<Animator>();
        IsFixed = false;
       
    }

    // Update is called once per frame
    void Update()
    {       


        if(IsFixed) return;     //如果被修复了，那么就不用执行下面的代码.
            
        
        changeTimer -= Time.deltaTime;
        if (changeTimer < 0){
            moveDirection *=-1; 
            changeTimer = changeDirectionTime;
        }

        Vector2 position = rbody.position;
        position.x += moveDirection.x * speed * Time.deltaTime;
        position.y += moveDirection.y * speed * Time.deltaTime;
        rbody.MovePosition(position);
        anim.SetFloat("MoveX",moveDirection.x);
        anim.SetFloat("MoveY",moveDirection.y);

    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
        PlayerController pc = other.gameObject.GetComponent<PlayerController>();
        if (pc != null) {
            pc.changeHealth(-1);
        }
    }
    //被修复的动画
    public void Fixed(){
        IsFixed = true;
        if(brokenEffect.isPlaying == true){
            brokenEffect.Stop();
        }
        anim.SetTrigger("Fiexed");
        AudioManager.instance.AudioPlay(audioClip);
        rbody.simulated = false;    //禁用物理      
        
    }
}
