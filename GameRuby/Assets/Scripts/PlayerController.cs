﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///控制角色移动、生命、动画等
///</summary>

public class PlayerController : MonoBehaviour
{
    public float speed = 5f;    //移动速度
    Rigidbody2D rbody;  //刚体
     
    private int maxHealth=5;      //玩家最大生命值
    private int currentHealth;  //玩家当前生命值

    //属性
    public int MyMaxHelath {get{return maxHealth;}}
    public int MyCurrentHealth{get {return currentHealth;}}


    private float invincibleTime = 2f;  //无敌时间
    private float invincibleTimer;  //无敌计时器
    private bool isInvincible;  //是否处于无敌时间
    // Start is called before the first frame update

    private Animator anim;
    private Vector2 lookDirection;      //朝向
    // private Vector2 moveVector;
    /////////////////音效////////////////////
    public AudioClip hitClip;   //受到攻击的音效
    public AudioClip launchClip;  //发射齿轮的音效
    public GameObject bulletPrefab;
        void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        invincibleTimer = 0;     //一开始时是可以受到伤害的
        currentHealth = 2;      //一开始时的生命值
        anim = GetComponent<Animator>();    //动画组件
        lookDirection = Vector2.down;
        UiManage.instance.UpdateHealthBar(currentHealth,maxHealth);     //更新血条
    }

    // Update is called once per frame
    void Update()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");
        
        Vector2 moveVector = new Vector2(moveX,moveY);  //定义移动方向
        if(moveVector.x != 0 || moveVector.y != 0){
            lookDirection = moveVector;
        }
        anim.SetFloat("Look X",lookDirection.x);
        anim.SetFloat("Look Y",lookDirection.y);
        anim.SetFloat("Speed",moveVector.magnitude);
        Vector2 position = rbody.position;
        // position.x += moveX *speed * Time.fixedDeltaTime;    //要+=才能将移动累加
        // position.y += moveY *speed * Time.fixedDeltaTime;
        position += moveVector *speed * Time.fixedDeltaTime;
        rbody.MovePosition(position);
      
        ///////////////// 计时器 ///////////////////////////////
        if (isInvincible){
            invincibleTimer -= Time.deltaTime;
            if (invincibleTimer < 0){
                isInvincible = false;   //计时器结束后，取消无敌状态。
            }//if end
                
           }//if end
    //=========================按下J键进行攻击====================================
        if (Input.GetKeyDown(KeyCode.J)){
            //生成
            anim.SetTrigger("Launch");
            GameObject bullet = Instantiate(bulletPrefab,rbody.position+Vector2.up*0.5f,Quaternion.identity);
         
            BulletController bc = bullet.GetComponent<BulletController>();
            if (bullet != null){
                AudioManager.instance.AudioPlay(launchClip);    //播放攻击音效
                bc.Move(lookDirection,300f);
            }
        }
    //======================按下E键与NPC进行交互==================================

    if(Input.GetKeyDown(KeyCode.E)){
        RaycastHit2D hit = Physics2D.Raycast(rbody.position,lookDirection,2F,LayerMask.GetMask("NPC")); 
        if (hit.collider != null){
                // Debug.Log("hit npc:"+hit.collider.gameObject.name);
                NPCManager npc = hit.collider.GetComponent<NPCManager>();
                if (npc != null){
                    npc.ShowDialog();
                }
        }
    }

    }//Update end
    ///<summay>
    ///改变玩家的生命值
    ///</summary>
    public void changeHealth(int amount){
        // Debug.Log(currentHealth+"/"+maxHealth);
        //如果玩家受到伤害，如果处于无敌状态，直接return
        if (amount<0){
            if(isInvincible){
              return;          
            }
            anim.SetTrigger("Hit");     //伤害动画
            AudioManager.instance.AudioPlay(hitClip);       //播放受伤音效
            //如果不是无敌状态，先将它设置为无敌状态，然后倒计时，然后改变血量。
            isInvincible = true;
            invincibleTimer = invincibleTime;
            
        }
        currentHealth = Mathf.Clamp(currentHealth + amount,0,maxHealth);    //约束生命值的最大最小值
        UiManage.instance.UpdateHealthBar(currentHealth,maxHealth);     //更新血条
        Debug.Log(currentHealth+"/"+maxHealth);
    }
}
